package com.bigfans.searchservice;

import com.bigfans.framework.Applications;
import com.bigfans.framework.CurrentUser;

/**
 * @author lichong
 * @create 2018-04-27 下午9:00
 **/
public class SearchApplications extends Applications {

    private static String functionalUserToken;
    private static CurrentUser functionalUser;

    public static void setFunctionalUser(CurrentUser functionalUser) {
        SearchApplications.functionalUser = functionalUser;
    }

    public static CurrentUser getFunctionalUser() {
        return functionalUser;
    }

    public static void setFunctionalUserToken(String functionalUserToken) {
        SearchApplications.functionalUserToken = functionalUserToken;
    }

    public static String getFunctionalUserToken() {
        return functionalUserToken;
    }
}
