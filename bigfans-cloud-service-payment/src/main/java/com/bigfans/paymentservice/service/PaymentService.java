package com.bigfans.paymentservice.service;

import com.bigfans.framework.dao.BaseService;
import com.bigfans.paymentservice.model.Order;
import com.bigfans.paymentservice.model.PayResult;
import com.bigfans.paymentservice.model.Payment;


/**
 * 
 * @Description:
 * @author lichong
 * 2015年4月5日下午6:34:47
 *
 */
public interface PaymentService extends BaseService<Payment> {
	
	Payment getByOrder(String uid, String orderId) throws Exception;
	
	int updateStatusToPaid(String paymentId) throws Exception;
	
	String generateF2FAlipayQrImage(Order order) throws Exception;

	Payment getByTradeNo(String tradeNo) throws Exception;
}
