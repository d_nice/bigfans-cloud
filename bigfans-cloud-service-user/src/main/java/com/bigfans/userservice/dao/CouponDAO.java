package com.bigfans.userservice.dao;

import com.bigfans.framework.dao.BaseDAO;
import com.bigfans.userservice.model.Coupon;

import java.util.List;

/**
 * 
 * @Description:
 * @author lichong
 * 2015年7月10日上午9:31:49
 *
 */
public interface CouponDAO extends BaseDAO<Coupon> {

	List<Coupon> listByUser(String userId);
	
	Coupon getByUser(String userId, String couponId);
	
}
