package com.bigfans.userservice.model;

import com.bigfans.userservice.model.entity.FavoritesEntity;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 
 * @Description:
 * @author lichong
 * @date Mar 10, 2016 3:23:26 PM
 *
 */
@Data
public class Favorites extends FavoritesEntity {

	private String username;
	private String prodName;
	private String prodImg;
	private BigDecimal price;
	private Integer favorCount;

}
