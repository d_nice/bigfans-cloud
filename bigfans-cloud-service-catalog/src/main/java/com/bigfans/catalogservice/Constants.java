package com.bigfans.catalogservice;

/**
 * @author lichong
 * @create 2018-04-21 上午9:29
 **/
public interface Constants {

    interface ERROR_CODE {
        int IMG_NOT_FOUND = 1401;
    }

}
