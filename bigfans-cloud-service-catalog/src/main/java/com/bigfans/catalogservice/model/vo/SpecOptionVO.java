package com.bigfans.catalogservice.model.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author lichong
 * @create 2018-07-28 下午1:32
 **/
@Data
public class SpecOptionVO implements Serializable {

    private static final long serialVersionUID = 1;

    private String id;
    private String name;
}
