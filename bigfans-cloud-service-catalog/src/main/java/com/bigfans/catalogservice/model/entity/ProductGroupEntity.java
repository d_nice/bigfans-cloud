package com.bigfans.catalogservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * 货品
 * 
 * @author lichong
 *
 */
@Data
@Table(name="ProductGroup")
public class ProductGroupEntity extends AbstractModel {

	public static final String COL_SHOWINHOME = "show_in_home";
	public static final String COL_NAME = "name";
	public static final String COL_PINYIN = "pinyin";

	private static final long serialVersionUID = -5042887778437645531L;
	
	public String getModule() {
		return "ProductGroup";
	}
	
	// 货号
	@Column(name="sn")
	protected String sn;
	// 商品名称
	@Column(name="name")
	protected String name;
	// 商品品牌
	@Column(name="brand_id")
	protected String brandId;
	// 产地
	@Column(name="origin")
	protected String origin;
	// 详细
	@Column(name="description")
	protected String description;
	// 商品类别
	@Column(name="category_id")
	protected String categoryId;
	
	@Override
	public String toString() {
		return "Product [name=" + name + ", origin=" + origin + "]";
	}

}
