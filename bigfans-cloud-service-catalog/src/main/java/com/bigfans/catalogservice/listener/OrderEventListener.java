package com.bigfans.catalogservice.listener;

import com.bigfans.catalogservice.service.sku.StockService;
import com.bigfans.framework.kafka.KafkaConsumerBean;
import com.bigfans.framework.kafka.KafkaListener;
import com.bigfans.model.event.order.OrderCanceledEvent;
import com.bigfans.model.event.order.OrderCreateFailureEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@KafkaConsumerBean
public class OrderEventListener {

    private Logger logger = LoggerFactory.getLogger(OrderEventListener.class);

    @Autowired
    private StockService stockService;

    @KafkaListener
    public void on(OrderCreateFailureEvent event){
        try{
            String orderId = event.getOrderId();
            stockService.revertOrderUsedStock(orderId);
        }catch (Exception e){
            logger.error(e.getMessage());
        }
    }

    @KafkaListener
    public void on(OrderCanceledEvent event){
        try{
            String orderId = event.getOrderId();
            stockService.revertOrderUsedStock(orderId);
        }catch (Exception e){
            logger.error(e.getMessage());
        }
    }

}
