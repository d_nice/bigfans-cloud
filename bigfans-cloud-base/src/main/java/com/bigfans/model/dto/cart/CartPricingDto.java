package com.bigfans.model.dto.cart;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lichong
 * @create 2018-02-16 下午2:34
 **/
@Data
public class CartPricingDto {

    private String userId;
    private List<CartItemDto> calculateItems = new ArrayList<>();

    public void addCartItem(CartItemDto cartItemDto){
        this.calculateItems.add(cartItemDto);
    }
}
