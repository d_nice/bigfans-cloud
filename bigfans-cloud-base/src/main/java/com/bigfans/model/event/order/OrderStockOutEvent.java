package com.bigfans.model.event.order;

import lombok.Data;

import java.util.Map;

/**
 * @author lichong
 * @create 2018-02-17 下午1:48
 **/
@Data
public class OrderStockOutEvent {

    private String orderId;
    private Map<String , Integer> prodQuantityMap;

    public OrderStockOutEvent(String orderId, Map<String, Integer> prodQuantityMap) {
        this.orderId = orderId;
        this.prodQuantityMap = prodQuantityMap;
    }
}
