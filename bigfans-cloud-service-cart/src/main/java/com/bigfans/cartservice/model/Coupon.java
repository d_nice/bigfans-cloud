package com.bigfans.cartservice.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 优惠劵
 * 
 * @author lichong
 *
 */
@Data
public class Coupon {

	protected String name;
	private boolean selectable;
	private boolean selected;

}
