package com.bigfans.cartservice.dao;

import com.bigfans.cartservice.model.Product;
import com.bigfans.framework.dao.BaseDAO;

public interface ProductDAO extends BaseDAO<Product> {

}
